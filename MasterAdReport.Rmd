---
title: "Weekly Ad Report"
author: "Chris"
date: "October 24, 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
	echo = FALSE,
	message = FALSE,
	warning = FALSE
)
```

```{r}
library(ggplot2)
library(knitr)
```


**Campaign Schedule FY2018**

<iframe align = "center" width = "1000" height = "1000" src="https://public.tableau.com/profile/christine.iyer#!/vizhome/CampaignTiming/Dashboard1?publish=yes"/>



**Advertising Performance**

The Analyst is tasked with monitoring and optimizing Advertising.

Every Tuesday, weekly performance ad reports have to be pulled. THis can be done in a number of ways. 

1. Check the email account. Some of the reports are automatically sent.

1. Go to the reporting sites directly to the websites. On the Admin Chrome browser, there is a bookmarked tab that is on the usmmarketing screen. This tab gives links to:

    + Twitter Ads Manager

    + Adwords Report:
        
        - [Report: USM_Marketing_Report_v1](https://adwords.google.com/reports/advanced/AdvancedReporting?__c=4018065042&__u=2674581083&authuser=0&__o=cues)
        

    + Facebook Report: 
    
        - [Report: WeeklyImportV3](https://business.facebook.com/ads/manager/account/ads/?act=1386547894957009&pid=p1&report_spec=6035102912905&business_id=10152837614281639)

    + DDM:
    
        - [Report: weeklyDCMImport](https://ddm.google.com/analytics/dfa/?defaultDs=2249728%3A9515#query-tool/templateList/2249728%3A9515/)
        - Don't use the geo report that is on the reports page.

    + Linkedin



__Where/how to store data for all vendors.__

Take this weekly data from Tuesday-Monday cycle and put in the shared dropbox folder (file:///C:/Users/usmmarketing.data/Dropbox/VendorData). Can share it right from the Dropbox on the admin computer to _____ and _____. 


If there has been a lag between reports, pull them from the sites, use the naming conventions Adwords_YYYY-mm-dd, DoubleClick_YYYY-mm-dd, Facebook_YYYY-mm-dd, Twitter_YYYY-mm-dd, put them in the Dropbox, and share. To see how the weekly reports look before any analysis, [click the Ads tab in the menu](https://marketingusm.bitbucket.io/Ads.html). 

__Relevant metrics and how to calculate them__


Michael developed a tableau workbook with calculated fields with the important metrics. (file:///C:/Users/ciyer/Documents/My Tableau Repository/Workbooks/Catch-Up.twbx)

[Also can be viewed here](https://public.tableau.com/views/Catch-Up_0/Story1?:embed=y&:display_count=yes)

[A cheatsheet of Digital Marketing definitions](https://marketingusm.bitbucket.io/Definitions.html)

+ Reach by Audience
+ Engagement by Audience


One final note, a separete but related foundation has been running Facebook Ads on and off. This is a little vague and I'm not sure if we have anything to monitor here. Tina may have some knowledge about this. 

###The final report will look something like this. 

<iframe align = "center" width = "1000" height = "1000" src="https://public.tableau.com/views/Catch-Up_0/Story1?:embed=y&:display_count=yes"/>




